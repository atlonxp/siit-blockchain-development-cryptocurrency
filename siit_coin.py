# Create a Cryptocurrency

########################################################################
# Importing the libraries
########################################################################
import datetime
import hashlib
import json
from urllib.parse import urlparse
from uuid import uuid4

import requests
from flask import Flask, jsonify, request


########################################################################
# Part 1 - Building a Blockchain
########################################################################
class Blockchain:

    def __init__(self):
        # Initialize the chain
        self.chain = []
        # Initialize the transactions
        self.transactions = []
        # Create the genesis block upon initialization
        self.create_block(proof=1, previous_hash='0')
        # Initialize the nodes
        self.nodes = set()

    def create_block(self, proof, previous_hash):
        # Create a block and append it to the chain
        block = {
            'index': len(self.chain) + 1,
            # last time, we have learnt about timestamp
            'timestamp': str(datetime.datetime.now()),
            'proof': proof,
            'previous_hash': previous_hash,
            'transactions': self.transactions
        }
        # Reset the current list of transactions after adding them to the block
        self.transactions = []
        # Append the block to the chain
        self.chain.append(block)
        return block

    def get_previous_block(self):
        # Return the last block in the chain
        return self.chain[-1]

    def proof_of_work(self, previous_proof):
        # Find a number p' such that hash(pp') contains leading 4 zeroes, where p is the previous p'
        new_proof = 1
        check_proof = False
        while check_proof is False:
            hash_operation = hashlib.sha256(str(new_proof ** 2 - previous_proof ** 2).encode()).hexdigest()
            if hash_operation[:4] == '0000':
                check_proof = True
            else:
                new_proof += 1
        return new_proof

    def hash(self, block):
        # Return the hash of the block
        encoded_block = json.dumps(block, sort_keys=True).encode()
        return hashlib.sha256(encoded_block).hexdigest()

    def is_chain_valid(self, chain):
        # Check if the chain is valid
        previous_block = chain[0]
        block_index = 1
        while block_index < len(chain):
            block = chain[block_index]
            if block['previous_hash'] != self.hash(previous_block):
                return False
            previous_proof = previous_block['proof']
            proof = block['proof']
            hash_operation = hashlib.sha256(str(proof ** 2 - previous_proof ** 2).encode()).hexdigest()
            if hash_operation[:4] != '0000':
                return False
            previous_block = block
            block_index += 1
        return True

    def add_transaction(self, sender, receiver, amount):
        # Add a transaction to the list of transactions
        self.transactions.append({
            'sender': sender,
            'receiver': receiver,
            'amount': amount
        })
        # Get the index of the block that will hold the next transaction
        previous_block = self.get_previous_block()
        return previous_block['index'] + 1

    def add_node(self, address):
        # Add a new node to the list of nodes using urlparse
        # Example: urlparse('http://127.0.0.1:5000')
        # Return : ParseResult(
        #              scheme='http', netloc='127.0.0.1:5000',
        #              path='', params='', query='', fragment=''
        #          )
        parsed_url = urlparse(address)
        self.nodes.add(parsed_url.netloc)

    def replace_chain(self):
        # Replace the chain by the longest one in the network
        #   > If the chain is not valid, do not replace it
        #   > If the chain is valid and is longer than the current chain, replace it
        network = self.nodes
        longest_chain = None

        # Initialize the longest chain with the current chain, the chain running on this current node (by this file)
        max_length = len(self.chain)

        # Loop through all the nodes in the network
        for node in network:
            # Get the chain of each node
            response = requests.get(f'http://{node}/get_chain')
            # Check if the response is valid
            if response.status_code == 200:
                length = response.json()['length']
                chain = response.json()['chain']

                # Check if the length of the chain is longer than the current longest chain
                if length > max_length and self.is_chain_valid(chain):
                    max_length = length
                    longest_chain = chain
        # Replace the chain if a longest chain is found
        if longest_chain:
            self.chain = longest_chain
            return True
        return False


########################################################################
# Part 2 - Mining our Blockchain
########################################################################

# Creating a Web App
app = Flask(__name__)

# Creating an address for the node
node_address = str(uuid4()).replace('-', '')

# Creating a Blockchain
blockchain = Blockchain()


# Mining a new block
@app.route('/mine_block', methods=['GET'])
def mine_block():
    previous_block = blockchain.get_previous_block()
    previous_proof = previous_block['proof']
    proof = blockchain.proof_of_work(previous_proof)
    previous_hash = blockchain.hash(previous_block)
    blockchain.add_transaction(sender=node_address, receiver='Hadelin', amount=1)
    block = blockchain.create_block(proof, previous_hash)
    response = {
        'message': 'Congratulations, you just mined a block!',
        'index': block['index'],
        'timestamp': block['timestamp'],
        'proof': block['proof'],
        'previous_hash': block['previous_hash'],
        'transactions': block['transactions']
    }
    return jsonify(response), 200


# Getting the full Blockchain
@app.route('/get_chain', methods=['GET'])
def get_chain():
    response = {
        'chain': blockchain.chain,
        'length': len(blockchain.chain)
    }
    return jsonify(response), 200


# Checking if the Blockchain is valid
@app.route('/is_valid', methods=['GET'])
def is_valid():
    is_valid = blockchain.is_chain_valid(blockchain.chain)
    if is_valid:
        response = {'message': 'All good. The Blockchain is valid.'}
    else:
        response = {'message': 'The Blockchain is not valid.'}
    return jsonify(response), 200


# Adding a new transaction to the Blockchain
@app.route('/add_transaction', methods=['POST'])
def add_transaction():
    # Get the transaction data from the POST request
    json = request.get_json()

    # Check if the transaction data is valid
    transaction_keys = ['sender', 'receiver', 'amount']
    if not all(key in json for key in transaction_keys):
        return 'Some elements of the transaction are missing', 400

    # Add the transaction to the Blockchain
    index = blockchain.add_transaction(json['sender'], json['receiver'], json['amount'])
    response = {'message': f'This transaction will be added to Block {index}'}
    return jsonify(response), 201


########################################################################
# Part 3 - Decentralizing our Blockchain
########################################################################

# Connecting new nodes
@app.route('/connect_node', methods=['POST'])
def connect_node():
    # Get the node data from the POST request
    json = request.get_json()
    nodes = json.get('nodes')
    if nodes is None:
        return "No node", 400
    for node in nodes:
        blockchain.add_node(node)
    response = {'message': 'All the nodes are now connected. The SIIT coin Blockchain now contains the following nodes:',
                'total_nodes': list(blockchain.nodes)}
    return jsonify(response), 201


# Replacing the chain by the longest chain if needed
@app.route('/replace_chain', methods=['GET'])
def replace_chain():
    is_chain_replaced = blockchain.replace_chain()
    if is_chain_replaced:
        response = {'message': 'The nodes had different chains so '
                               'the chain was replaced by the longest one.',
                    'new_chain': blockchain.chain}
    else:
        response = {'message': 'All good. The chain is the largest one.',
                    'actual_chain': blockchain.chain}
    return jsonify(response), 200


# Running the app
app.run(host='0.0.0.0', port=5000)
