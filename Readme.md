# SIIT: Blockchain Development

## Cryptocurrency - Tutorial

### Install Virtual Environment

1. Download and install the appropriate version of Anaconda with Python 3.7+:
- Windows: https://conda.io/projects/conda/en/latest/user-guide/install/windows.html
- macOS: https://conda.io/projects/conda/en/latest/user-guide/install/macos.html
- Linux: https://conda.io/projects/conda/en/latest/user-guide/install/linux.html
2. Create a new Conda virtual environment
- Open the Anaconda Prompt by searching for it in the Start Menu (Windows) or typing "conda" in the command line (macOS/Linux).
- Type `conda create -n {your_env_name} python={version}` where `{your_env_name}` is the name of your environment, and `{version}` is the version of Python you want to use (e.g., 3.7).
3. Activate your Conda environment
- To activate your virtual environment, type `conda activate {your_env_name}`, where `{your_env_name}` is the name of your environment.
4. [Optional] Install packages into your Conda environment
- To install additional packages, you can use the `conda install` command. For example, to install the requests package, type `conda install requests`.
- You can use the `pip install` command. For example, to install the requests package, type `pip install requests`. 
  * In this case, we will use the `pip install` command.
  * Since this project has a `requirements.txt` file, so we can install all the packages by typing `pip install -r requirements.txt`
5. Deactivate your Conda environment 
   * To deactivate your virtual environment, type “conda deactivate”.

### Run the code

1. Clone the repository
2. Open the Anaconda Prompt by searching for it in the Start Menu (Windows) or typing "conda" in the command line (macOS/Linux).
3. Activate your Conda environment
    * if you have not installed libraries, please install libraries by typing `pip install -r requirements.txt`
4. Navigate to the project folder
5. Run the code by typing `python siit_coin.py` to run the first node on port 5000
6. Open a new terminal and repeat steps 3-5 to run the second node on port 5001
7. Open a new terminal and repeat steps 3-5 to run the third node on port 5002
8. Open a new terminal and repeat steps 3-5 to run the fourth node on port 5003
9. Now you can use Postman to test the API endpoints
10. To stop the nodes, press `Ctrl+C` in each terminal


### Test endpoints with Postman
1. Open Postman
2. Test the API endpoints by sending requests to the nodes
3. Test Get Chain 
   * Call GET http://127.0.0.1:5000/get_chain to get the full blockchain on node (port 5000)
   * Call GET http://127.0.0.1:5001/get_chain to get the full blockchain on node (port 5001)
   * Call GET http://127.0.0.1:5002/get_chain to get the full blockchain on node (port 5002)
   * Call GET http://127.0.0.1:5003/get_chain to get the full blockchain on node (port 5003)
   * ![get-chain.png](assets%2Fget-chain.png)
4. Now they are all working, we can connect all nodes together
5. Connect all nodes together
   * **Node (port 5000)**, call POST http://127.0.0.1:5000/connect_node with body in JSON format: 
   
   ```
   {
       "nodes": ["http://127.0.0.1:5001", 
                 "http://127.0.0.1:5002", 
                 "http://127.0.0.1:5003"]
   }
   ``` 
    * **Node (port 5001)**, call POST http://127.0.0.1:5001/connect_node with body in JSON format:

   ```
   {
       "nodes": ["http://127.0.0.1:5000", 
                 "http://127.0.0.1:5002", 
                 "http://127.0.0.1:5003"]
   }
   ``` 
    * **Node (port 5002)**, call POST http://127.0.0.1:5002/connect_node with body in JSON format:

   ```
   {
       "nodes": ["http://127.0.0.1:5000", 
                 "http://127.0.0.1:5001", 
                 "http://127.0.0.1:5003"]
   }
   ``` 
    * **Node (port 5003)**, call POST http://127.0.0.1:5003/connect_node with body in JSON format:

   ```
   {
       "nodes": ["http://127.0.0.1:5000", 
                 "http://127.0.0.1:5001", 
                 "http://127.0.0.1:5002"]
   }
   ```
   ![connect-node.png](assets%2Fconnect-node.png)
6. Now, you can try mine block on one of them, and see how it goes.
   * You may notice right away that the blockchain is not synchronized.
   * To synchronize the blockchain, we need to replace the chain on each node with the longest valid chain.
   * ![mine-block.png](assets%2Fmine-block.png)
7. Then, we can replace the chain on each node with the longest valid chain.
   * **Node (port 5000)**, call GET http://127.0.0.1:5000/replace_chain
   * Remember, you need to run this command on each node.
   * ![replace-chain-2.png](assets%2Freplace-chain-2.png)
8. Since all chains are synchronized, you can try to add some transactions and mine block on one of them, and see how it goes.
9. That's it. You have successfully created a blockchain network with 4 nodes.
9. To stop the nodes, press `Ctrl+C` in each terminal